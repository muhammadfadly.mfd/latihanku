package com.muhammadfadhlidzilikram.aplikasialquran;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.card.MaterialCardView;

import java.util.Calendar;
import java.util.Date;

public class menu_utama extends Activity implements View.OnClickListener {
    MaterialCardView cvkeluar,cvcrud,cvalquran,cvinfo;
    TextView tvToday;

    String hariIni;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_utama);

        cvalquran = findViewById(R.id.alquran);
        cvcrud = findViewById(R.id.crud);
        cvinfo = findViewById(R.id.info);
        cvkeluar = findViewById(R.id.keluar);

        cvalquran.setOnClickListener(this);
        cvcrud.setOnClickListener(this);
        cvinfo.setOnClickListener(this);
        cvkeluar.setOnClickListener(this);

        tvToday = findViewById(R.id.tvDate);
        Date dateNow = Calendar.getInstance().getTime();
        hariIni = (String) DateFormat.format("EEEE", dateNow);
        if (hariIni.equalsIgnoreCase("sunday")) {
            hariIni = "Minggu";
        } else if (hariIni.equalsIgnoreCase("monday")) {
            hariIni = "Senin";
        } else if (hariIni.equalsIgnoreCase("tuesday")) {
            hariIni = "Selasa";
        } else if (hariIni.equalsIgnoreCase("wednesday")) {
            hariIni = "Rabu";
        } else if (hariIni.equalsIgnoreCase("thursday")) {
            hariIni = "Kamis";
        } else if (hariIni.equalsIgnoreCase("friday")) {
            hariIni = "Jumat";
        } else if (hariIni.equalsIgnoreCase("saturday")) {
            hariIni = "Sabtu";
        }

        getToday();
    }

    public void onBackPressed() {
        CFAlertDialog.Builder alertDialogBuilder = new CFAlertDialog.Builder(this);
        alertDialogBuilder.setIcon(R.drawable.unimal);
        alertDialogBuilder.setTitle("Perhatian");
        alertDialogBuilder.setMessage("apakah Kamu akan keluar dari aplikasi?")
                .addButton("IYA", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> {
                    dialog.dismiss();
                    System.exit(0);
                });

        alertDialogBuilder.create().show();
    }

    private void getToday() {
        Date date = Calendar.getInstance().getTime();
        String tanggal = (String) DateFormat.format("d", date);
        String monthNumber = (String) DateFormat.format("M", date);
        String year = (String) DateFormat.format("yyyy", date);

        int month = Integer.parseInt(monthNumber);
        String bulan = null;
        if (month == 1) {
            bulan = "Januari";
        } else if (month == 2) {
            bulan = "Februari";
        } else if (month == 3) {
            bulan = "Maret";
        } else if (month == 4) {
            bulan = "April";
        } else if (month == 5) {
            bulan = "Mei";
        } else if (month == 6) {
            bulan = "Juni";
        } else if (month == 7) {
            bulan = "Juli";
        } else if (month == 8) {
            bulan = "Agustus";
        } else if (month == 9) {
            bulan = "September";
        } else if (month == 10) {
            bulan = "Oktober";
        } else if (month == 11) {
            bulan = "November";
        } else if (month == 12) {
            bulan = "Desember";
        }
        String formatFix = hariIni + ", " + tanggal + " " + bulan + " " + year;
        tvToday.setText(formatFix);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.alquran) {
            Intent intent = new Intent(menu_utama.this, com.muhammadfadhlidzilikram.aplikasialquran.surah_activity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.crud) {
            Intent intent = new Intent(menu_utama.this, com.muhammadfadhlidzilikram.aplikasialquran.MainActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.info) {
            Intent intent = new Intent(menu_utama.this, com.muhammadfadhlidzilikram.aplikasialquran.info_activity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.keluar) {

            CFAlertDialog.Builder alertDialogBuilder = new CFAlertDialog.Builder(this);
            alertDialogBuilder.setIcon(R.drawable.unimal);
            alertDialogBuilder.setTitle("Perhatian");
            alertDialogBuilder.setMessage("apakah Kamu akan keluar dari aplikasi?")
                    .addButton("IYA", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> {
                        dialog.dismiss();
                        System.exit(0);
                    });

            alertDialogBuilder.create().show();
        }
    }
}
