package com.muhammadfadhlidzilikram.aplikasialquran.awal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.muhammadfadhlidzilikram.aplikasialquran.R;
import com.muhammadfadhlidzilikram.aplikasialquran.menu_utama;


public class splasscren extends Activity {

    private int waktu_loading = 1000;

    //4000=4 detik
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splasscren);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//setelah loading maka akan langsung berpindah ke home activity
                Intent home = new Intent(com.muhammadfadhlidzilikram.aplikasialquran.awal.splasscren.this,
                        menu_utama.class);
                startActivity(home);
                finish();
            }
        }, waktu_loading);
    }
}
