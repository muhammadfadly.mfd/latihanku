package com.muhammadfadhlidzilikram.aplikasialquran;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;



import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.muhammadfadhlidzilikram.aplikasialquran.adapter.AdapterData;

import com.muhammadfadhlidzilikram.aplikasialquran.api.APIrequestData;
import com.muhammadfadhlidzilikram.aplikasialquran.api.RetroServer;

import com.muhammadfadhlidzilikram.aplikasialquran.crud.tambahActivity;
import com.muhammadfadhlidzilikram.aplikasialquran.model.ModelSurah;
import com.muhammadfadhlidzilikram.aplikasialquran.model.ResponseModel;




import java.util.ArrayList;
import java.util.List;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {
    private RecyclerView rvData;
    private RecyclerView.Adapter adData;
    private RecyclerView.LayoutManager lmData;
    private List<ModelSurah> listData = new ArrayList<>();
    private SwipeRefreshLayout srlData;
    private ProgressBar pbData;
    private FloatingActionButton fabTambah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvData = findViewById(R.id.rv_data);
        srlData = findViewById(R.id.srl_data);
        pbData = findViewById(R.id.pb_data);
        fabTambah = findViewById(R.id.fabtambah);

setupToolbar();


        lmData = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvData.setLayoutManager(lmData);
        //retriverData();

        srlData.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srlData.setRefreshing(true);
                retriverData();
                srlData.setRefreshing(false);
            }
        });

        fabTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, tambahActivity.class));
            }
        });
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.tbSurah);
        toolbar.setTitle("Referensi Surah");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }



    @Override
    protected void onResume() {
        super.onResume();
        retriverData();
    }

    public void retriverData(){
        pbData.setVisibility(View.VISIBLE);
        APIrequestData ardData = RetroServer.konekRetrofit().create(APIrequestData.class);
        Call<ResponseModel> tampilData = ardData.ardRetrieveData();
        tampilData.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
//                int kode = response.body().getKode();
                String pesan = response.body().getPesan();

                Toast.makeText(MainActivity.this, " pesan :" +pesan, Toast.LENGTH_SHORT).show();

                listData= response.body().getData();

                adData= new AdapterData( MainActivity.this, listData);
                rvData.setAdapter(adData);
                adData.notifyDataSetChanged();

                pbData.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Gagal Menghubungi Server : "+t.getMessage(), Toast.LENGTH_SHORT).show();
                pbData.setVisibility(View.INVISIBLE);
            }
        });
    }
}