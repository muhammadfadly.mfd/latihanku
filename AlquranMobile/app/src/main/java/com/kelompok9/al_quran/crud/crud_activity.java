package com.kelompok9.al_quran.crud;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.kelompok9.al_quran.R;
import com.kelompok9.al_quran.api.APIrequestData;
import com.kelompok9.al_quran.api.RetroServer;
import com.kelompok9.al_quran.model.ResponseModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class crud_activity extends AppCompatActivity {
    private int yId;
    private String yNama, yUrl, yUrlimage,yNosurah;
    private EditText etNama, etUrl, etUrlImage, etNosurah;
    private Button btnUbah;
    private String xNama, xUrl, xUrlimage,xNosurah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah);


        Intent terima = getIntent();
        yId = terima.getIntExtra("xId",-1);
        yNama = terima.getStringExtra("xNama");
        yUrl = terima.getStringExtra("xUrl");
        yUrlimage = terima.getStringExtra("xUrlimage");
        yNosurah = terima.getStringExtra("xNosurah");

        etNama = findViewById(R.id.et_nama);
        etUrl = findViewById(R.id.et_url);
        etUrlImage = findViewById(R.id.et_urlimage);
        etNosurah = findViewById(R.id.et_nosurah);
        btnUbah = findViewById(R.id.btn_simpan);

        etNama.setText(yNama);
        etUrl.setText(yUrl);
        etUrlImage.setText(yUrlimage);
        etNosurah.setText(yNosurah);

        btnUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                xNama = etNama.getText().toString();
                xUrl = etUrl.getText().toString();
                xUrlimage = etUrlImage.getText().toString();
                xNosurah = etNosurah.getText().toString();

                updateData();
            }
        });


    }

    public void updateData(){
        APIrequestData ardData = RetroServer.konekRetrofit().create(APIrequestData.class);
        Call<ResponseModel> ubahData = ardData.ardUpdateData(yId, xNama, xUrl, xUrlimage,xNosurah);

        ubahData.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
//                int kode = response.body().getKode();
                String pesan = response.body().getPesan();

                Toast.makeText(crud_activity.this, "pesan : "+pesan, Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(crud_activity.this, "Gagal menghubungi server " +t.getMessage(), Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }



}