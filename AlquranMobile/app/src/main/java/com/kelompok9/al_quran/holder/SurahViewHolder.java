package com.kelompok9.al_quran.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.kelompok9.al_quran.R;

public class SurahViewHolder extends RecyclerView.ViewHolder {
    public ImageView image;
    public TextView namaSurah;
    public TextView noSurah;
    public MaterialCardView cvSurah;
    public View view;

    public SurahViewHolder(View view){
        super(view);

        cvSurah = view.findViewById(R.id.cvSurah);
        image = view.findViewById(R.id.image);
        namaSurah = view.findViewById(R.id.namaSurah);
        noSurah = view.findViewById(R.id.noSurah);
        this.view=view;

    }
}
