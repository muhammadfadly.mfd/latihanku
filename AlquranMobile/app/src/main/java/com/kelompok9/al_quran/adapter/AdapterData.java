package com.kelompok9.al_quran.adapter;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.kelompok9.al_quran.MainActivity;
import com.kelompok9.al_quran.R;
import com.kelompok9.al_quran.api.APIrequestData;
import com.kelompok9.al_quran.api.RetroServer;
import com.kelompok9.al_quran.crud.crud_activity;
import com.kelompok9.al_quran.model.ModelSurah;
import com.kelompok9.al_quran.model.ResponseModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AdapterData extends RecyclerView.Adapter<AdapterData.HolderData>{
    private Context ctx;
    private List<ModelSurah> listData;
    private List<ModelSurah> listSurah;
    private int idsurah;

    public AdapterData(Context ctx, List<ModelSurah> listData){
        this.ctx = ctx;
        this.listData = listData;
    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item, parent, false);
        HolderData holder = new HolderData(layout);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull HolderData holder, int position) {
        ModelSurah dm = listData.get(position);

        holder.tvId.setText(String.valueOf(dm.getId()));
        holder.tvNama.setText(dm.getNama_surah());
//        holder.tvUrl.setText(dm.getUrl());
//        holder.tvUrlimage.setText(dm.getUrlimage());
        holder.tvNosurah.setText(String.valueOf(dm.getNosurah()));

    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class HolderData extends RecyclerView.ViewHolder {
        TextView tvId, tvNama, tvUrl, tvUrlimage, tvNosurah;

        public HolderData(@NonNull View itemView) {
            super(itemView);

            tvId = itemView.findViewById(R.id.tv_id);
            tvNama = itemView.findViewById(R.id.tv_nama);
//            tvUrl = itemView.findViewById(R.id.tv_url);
//            tvUrlimage = itemView.findViewById(R.id.tv_urlimage);
            tvNosurah = itemView.findViewById(R.id.tv_nosurah);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    AlertDialog.Builder dialogpesan = new AlertDialog.Builder(ctx);
                    dialogpesan.setMessage("Pilih Operasi Yang akan dilakukan");
                    dialogpesan.setCancelable(true);
                    dialogpesan.setTitle("Peringatan");
                    dialogpesan.setIcon(R.drawable.quran);

                    idsurah = Integer.parseInt((tvId.getText()).toString());

                    dialogpesan.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        deletedata();
                        dialog.dismiss();
                            ((MainActivity)ctx).retriverData();
                        }
                    });

                    dialogpesan.setNegativeButton("Ubah", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            getData();
                        }
                    });

                    dialogpesan.show();

                    return false;
                }
            });
        }

        public void deletedata(){
            APIrequestData ardData = RetroServer.konekRetrofit().create(APIrequestData.class);
            Call<ResponseModel> hapusData = ardData.ardDeleteData(idsurah);

            hapusData.enqueue(new Callback<ResponseModel>() {
                @Override
                public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                    String pesan = response.body().getPesan();


                    Toast.makeText(ctx," pesan "+pesan, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    Toast.makeText(ctx,"Gagal menghubungi Server : " +t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }

        private void getData(){
            APIrequestData ardData = RetroServer.konekRetrofit().create(APIrequestData.class);
            Call<ResponseModel> ambilData = ardData.ardGetData(idsurah);

            ambilData.enqueue(new Callback<ResponseModel>() {
                @Override
                public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                    String pesan = response.body().getPesan();

                    listSurah = response.body().getData();

                    int varId = listSurah.get(0).getId();
                    String varNama = listSurah.get(0).getNama_surah();
                    String varUrl = listSurah.get(0).getUrl();
                    String varUrlimage = listSurah.get(0).getUrlimage();
                    String varNosurah = listSurah.get(0).getNosurah();

//                    Toast.makeText(ctx,"kode" +kode+ " | pesan "+pesan,Toast.LENGTH_SHORT).show();

                    Intent kirim = new Intent(ctx, crud_activity.class);
                    kirim.putExtra("xId", varId);
                    kirim.putExtra("xNama", varNama);
                    kirim.putExtra("xUrl", varUrl);
                    kirim.putExtra("xUrlimage", varUrlimage);
                    kirim.putExtra("xNosurah", varNosurah);
                    ctx.startActivity(kirim);
                }

                @Override
                public void onFailure(Call<ResponseModel> call, Throwable t) {
                    Toast.makeText(ctx,"Gagal menghubungi Server : " +t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }

    }
}
