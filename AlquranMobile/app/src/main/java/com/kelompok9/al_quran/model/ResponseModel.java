package com.kelompok9.al_quran.model;

import java.util.List;

public class ResponseModel {

    private String pesan;
    private List<ModelSurah> data;


    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public List<ModelSurah> getData() {
        return data;
    }

    public void setData(List<ModelSurah> data) {
        this.data = data;
    }
}

