package com.kelompok9.al_quran.crud;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.kelompok9.al_quran.R;
import com.kelompok9.al_quran.api.APIrequestData;
import com.kelompok9.al_quran.api.RetroServer;
import com.kelompok9.al_quran.model.ResponseModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class tambahActivity extends AppCompatActivity {
    private EditText etNama, etUrl, etUrlImage, etNosurah;
    private Button btnsimpan;
    private String nama_surah, url, urlimage, nosurah;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah);

        etNama = findViewById(R.id.et_nama);
        etUrl = findViewById(R.id.et_url);
        etUrlImage = findViewById(R.id.et_urlimage);
        etNosurah = findViewById(R.id.et_nosurah);
        btnsimpan = findViewById(R.id.btn_simpan);

        btnsimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nama_surah = etNama.getText().toString();
                url = etUrl.getText().toString();
                urlimage = etUrlImage.getText().toString();
                nosurah = etNosurah.getText().toString();

                if (nama_surah.trim().equals("")) {
                    etNama.setError("Nama Surah Harus Diisi");
                } else if (url.trim().equals("")) {
                    etUrl.setError("Alamat URL Harus Diisi");
                } else if (urlimage.trim().equals("")) {
                    etUrlImage.setError("Link Gambar Harus Diisi Harus Diisi");
                }
//                else if (nosurah.trim().equals("")){
//                    etNosurah.setError("No. Surah Harus Diisi");
//                }
                else {
                    createData();
                }

            }
        });
    }

    private void createData(){
        APIrequestData ardData = RetroServer.konekRetrofit().create(APIrequestData.class);
        Call<ResponseModel> simpanData = ardData.ardCreateData(nama_surah, url, urlimage, nosurah);

        simpanData.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
//                int kode = response.body().getKode();
                String pesan = response.body().getPesan();
//
                Toast.makeText(tambahActivity.this, " Pesan : "+pesan, Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(tambahActivity.this, "Gagal menghubungi server " +t.getMessage(), Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}