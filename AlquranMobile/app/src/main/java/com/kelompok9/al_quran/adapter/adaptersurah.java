package com.kelompok9.al_quran.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.kelompok9.al_quran.R;
import com.kelompok9.al_quran.holder.SurahViewHolder;
import com.kelompok9.al_quran.model.ModelSurah;


import java.util.List;

public class adaptersurah extends RecyclerView.Adapter<SurahViewHolder> {

    public List<ModelSurah> androidlist;
    private Context mContext;
    private adaptersurah.onSelectData onSelectData;


    public interface onSelectData{
        void onSelected(ModelSurah mdlSurah);
    }

    public adaptersurah(Context context, List<ModelSurah> android, adaptersurah.onSelectData onSelectData){
        this.mContext = context;
        this.androidlist = android;
        this.onSelectData = onSelectData;
    }

    @NonNull
    @Override
    public SurahViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_item_surah, viewGroup, false);
        return new SurahViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SurahViewHolder viewHolder, int i ){

final ModelSurah surah= androidlist.get(i);

        Glide.with(mContext)
                .load(surah.getUrlimage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.quran)
                //.transform(new CenterInside(), new RoundedCorners(30))
                .apply(RequestOptions.bitmapTransform(new RoundedCorners(30)))
                .into(viewHolder.image);

        viewHolder.namaSurah.setText(surah.getNama_surah());
        viewHolder.noSurah.setText(surah.getNosurah());
        viewHolder.cvSurah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectData.onSelected(surah);
            }
        });
    }

    @Override
    public int getItemCount() { return androidlist.size();
    }
}
