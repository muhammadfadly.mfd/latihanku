package com.kelompok9.al_quran.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroServer {
    private static final String baseURL = "https://androidstack.my.id/alquran/webservice/api/";

    private static Retrofit retro;

    public static Retrofit konekRetrofit(){
        if ((retro == null)){
            retro = new Retrofit.Builder()
                    .baseUrl(baseURL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retro;
    }
}

