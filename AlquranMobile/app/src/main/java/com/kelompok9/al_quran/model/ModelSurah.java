package com.kelompok9.al_quran.model;

public class ModelSurah {

    private int id;
    private String nama_surah;
    private String url;
    private String urlimage;
    private String nosurah;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama_surah() {
        return nama_surah;
    }

    public void setNama_surah(String nama_surah) {
        this.nama_surah = nama_surah;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrlimage() {
        return urlimage;
    }

    public void setUrlimage(String urlimage) {
        this.urlimage = urlimage;
    }

    public String getNosurah() {
        return nosurah;
    }

    public void setNosurah(String nosurah) {
        this.nosurah = nosurah;
    }
}
