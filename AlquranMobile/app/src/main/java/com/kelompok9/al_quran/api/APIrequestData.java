package com.kelompok9.al_quran.api;

import com.kelompok9.al_quran.model.ResponseModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface APIrequestData {

    @GET("retrieve.php")
    Call<ResponseModel> ardRetrieveData();

    @FormUrlEncoded
    @POST("add.php")
    Call<ResponseModel> ardCreateData(
            @Field("nama_surah") String nama_surah,
            @Field("url") String url,
            @Field("urlimage") String urlimage,
            @Field("nosurah") String nosurah
    );

    @FormUrlEncoded
    @POST("delete.php")
    Call<ResponseModel> ardDeleteData(
            @Field("id") int id
    );

    @FormUrlEncoded
    @POST("get.php")
    Call<ResponseModel> ardGetData(
            @Field("id") int id
    );

    @FormUrlEncoded
    @POST("update.php")
    Call<ResponseModel> ardUpdateData(
            @Field("id") int id,
            @Field("nama_surah") String nama_surah,
            @Field("url") String url,
            @Field("urlimage") String urlimage,
            @Field("nosurah") String nosurah
    );
}

