<?php
require("koneksi.php");
$perintah = "SELECT * FROM tbl_surah ORDER BY nosurah ASC ";
$eksekusi = mysqli_query($konek, $perintah);
$cek = mysqli_affected_rows($konek);

if($cek > 0){
  
    $response["pesan"] = "Data Tersedia";
    $response["data"]= array();

    while($ambil = mysqli_fetch_object($eksekusi)){
        $F["id"] = $ambil->id;
        $F["nama_surah"] = $ambil->nama_surah;
        $F["tanggal"] = $ambil->tanggal;
        $F["url"] = $ambil->url;
        $F["urlimage"] = $ambil->urlimage;
        $F["nosurah"] = $ambil->nosurah;

        array_push($response["data"], $F );
    }
    
}
else{
    $response["pesan"] = "Data Tidak Tersedia";
}

echo json_encode($response);
mysqli_close($konek);