<?php
require("../../koneksi.php");

$response = array();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    
    $id = $_POST["id"];
  

    $perintah = "SELECT * FROM tbl_surah Where id = '$id'";
    $eksekusi = mysqli_query($konek, $perintah);
    $cek      = mysqli_affected_rows($konek);

    if($cek > 0){
        $response["kode"] = 1;
        $response["pesan"] = "Data Tersedia";
        $response["data"] = array();

        while($ambil = mysqli_fetch_object($eksekusi)){
            $F["id"] = $ambil->id;
            $F["nama_surah"] = $ambil->nama_surah;
            $F["url"] = $ambil->url;
            $F["urlimage"] = $ambil->urlimage;
            $F["nosurah"] = $ambil->nosurah;
    
            array_push($response["data"], $F );
        }
    }
    else{
        $response["kode"] = 0;
        $response["pesan"] = "Tidak tersedia";
    }
} 
else{
    $response["kode"] = 0;
    $response["pesan"] = "tidak ada Post Data";
}

echo json_encode($response);
mysqli_close($konek);
