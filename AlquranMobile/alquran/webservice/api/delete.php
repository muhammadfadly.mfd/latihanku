<?php
require("../../koneksi.php");

$response = array();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    
    $id = $_POST["id"];

    $perintah = "DELETE FROM tbl_surah Where id = '$id'";
    $eksekusi = mysqli_query($konek, $perintah);
    $cek      = mysqli_affected_rows($konek);

    if($cek > 0){
        $response["kode"] = 1;
        $response["pesan"] = "Data Dihapus";
    }
    else{
        $response["kode"] = 0;
        $response["pesan"] = "Gagal Dihapus";
    }
} 
else{
    $response["kode"] = 0;
    $response["pesan"] = "tidak ada Post Data";
}

echo json_encode($response);
mysqli_close($konek);
