-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 29 Jun 2021 pada 14.03
-- Versi Server: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `al-quran`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_surah`
--

CREATE TABLE IF NOT EXISTS `tbl_surah` (
`id` int(11) NOT NULL,
  `nama_surah` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `url` text NOT NULL,
  `urlimage` text NOT NULL,
  `nosurah` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_surah`
--

INSERT INTO `tbl_surah` (`id`, `nama_surah`, `tanggal`, `url`, `urlimage`, `nosurah`) VALUES
(1, 'Al-Fatihah', '2021-06-03', 'https://www.youtube.com/watch?v=zkPag_vE-qs', 'https://bimbinganislam.com/wp-content/uploads/2018/12/Kenikmatan-Al-Quran-bimbinganislam.jpg', 1),
(9, 'cdd', '0000-00-00', 'fadli', 'fadli', 7),
(10, 'fs', '0000-00-00', 'fasdf', 'sdfd', 3),
(12, 'fs', '0000-00-00', 'fasdf', 'sdfd', 6),
(17, 'bfdb', '0000-00-00', 'fdbhdf', 'dfbdfb', 7768),
(18, 'fadlii', '0000-00-00', 'fadli', 'dgd', 56),
(20, 'eee', '0000-00-00', 'srs', 'cgcg', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_surah`
--
ALTER TABLE `tbl_surah`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `nosurah` (`nosurah`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_surah`
--
ALTER TABLE `tbl_surah`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
