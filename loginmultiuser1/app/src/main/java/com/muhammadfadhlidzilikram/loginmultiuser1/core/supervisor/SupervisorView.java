package com.muhammadfadhlidzilikram.loginmultiuser1.core.supervisor;


import com.muhammadfadhlidzilikram.loginmultiuser1.model.MainModel;

/**
 * Created by isfaaghyth on 8/10/17.
 * github: @isfaaghyth
 */

public interface SupervisorView {
    void onSuccess(MainModel body);
    void onError(String message);
}
