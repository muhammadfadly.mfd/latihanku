package com.muhammadfadhlidzilikram.loginmultiuser1.core.bigboss;


import com.muhammadfadhlidzilikram.loginmultiuser1.model.MainModel;

/**
 * Created by isfaaghyth on 8/10/17.
 * github: @isfaaghyth
 */

public interface BigbossView {
    void onSuccess(MainModel body);
    void onError(String message);
}
