package com.muhammadfadhlidzilikram.loginmultiuser1.core.login;


import androidx.appcompat.app.AppCompatActivity;

import com.muhammadfadhlidzilikram.loginmultiuser1.model.LoginModel;


/**
 * Created by isfaaghyth on 8/10/17.
 * github: @isfaaghyth
 */

public interface LoginView {
    void onSuccess(LoginModel result);
    void onError(String message);
    AppCompatActivity getActivity();
}
