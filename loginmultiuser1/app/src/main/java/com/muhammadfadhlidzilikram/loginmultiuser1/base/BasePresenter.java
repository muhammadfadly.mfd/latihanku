package com.muhammadfadhlidzilikram.loginmultiuser1.base;


import com.muhammadfadhlidzilikram.loginmultiuser1.network.BosqClient;
import com.muhammadfadhlidzilikram.loginmultiuser1.network.RouteServices;
import com.muhammadfadhlidzilikram.loginmultiuser1.util.CacheManager;

/**
 * Created by isfaaghyth on 8/10/17.
 * github: @isfaaghyth
 */

public class BasePresenter<V> {
    public V view;
    private RouteServices service;

    public void attachView(V view) {
        this.view = view;
        service = BosqClient.getClient().create(RouteServices.class);
    }

    public void dettachView() {
        this.view = null;
    }

    public RouteServices getService() {
        return service;
    }
    
    protected String getToken() {
        return CacheManager.grabString("token");
    }

}
