package com.muhammadfadhlidzilikram.loginmultiuser1.core.bigboss;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;


import com.muhammadfadhlidzilikram.loginmultiuser1.R;
import com.muhammadfadhlidzilikram.loginmultiuser1.base.BaseActivity;
import com.muhammadfadhlidzilikram.loginmultiuser1.core.login.LoginActivity;
import com.muhammadfadhlidzilikram.loginmultiuser1.model.MainModel;
import com.muhammadfadhlidzilikram.loginmultiuser1.util.CacheManager;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by isfaaghyth on 8/10/17.
 * github: @isfaaghyth
 */
public class BigbossActivity extends BaseActivity<BigbossPresenter> implements BigbossView {

    @BindView(R.id.txt_name) TextView txtName;

    @Override protected BigbossPresenter createPresenter() {
        return new BigbossPresenter(this);
    }

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding(R.layout.activity_bigboss);
        txtName.setText(CacheManager.grabString("name"));
        presenter.getData();
    }

    @OnClick(R.id.btn_logout)
    public void onLogoutClicked() {
        CacheManager.save("login", false);
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override public void onSuccess(MainModel body) {
        Toast.makeText(this, body.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override public void onError(String message) {
        Log.e("TAG", message);
    }
}