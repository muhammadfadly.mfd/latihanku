package com.muhammadfadhlidzilikram.loginmultiuser.core.supervisor;


import com.muhammadfadhlidzilikram.loginmultiuser.model.MainModel;

/**
 * Created by isfaaghyth on 8/10/17.
 * github: @isfaaghyth
 */

public interface SupervisorView {
    void onSuccess(MainModel body);
    void onError(String message);
}
