package com.muhammadfadhlidzilikram.loginmultiuser.core.login;


import androidx.appcompat.app.AppCompatActivity;

import com.muhammadfadhlidzilikram.loginmultiuser.model.LoginModel;

/**
 * Created by isfaaghyth on 8/10/17.
 * github: @isfaaghyth
 */

public interface LoginView {
    void onSuccess(LoginModel result);
    void onError(String message);
    AppCompatActivity getActivity();
}
