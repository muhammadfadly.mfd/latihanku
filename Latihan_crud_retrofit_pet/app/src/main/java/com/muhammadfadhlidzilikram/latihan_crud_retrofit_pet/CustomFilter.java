package com.muhammadfadhlidzilikram.latihan_crud_retrofit_pet;

import android.widget.Filter;

import java.util.ArrayList;

public class CustomFilter extends Filter {

    com.muhammadfadhlidzilikram.latihan_crud_retrofit_pet.Adapter adapter;
    ArrayList<com.muhammadfadhlidzilikram.latihan_crud_retrofit_pet.Pets> filterList;

    public CustomFilter(ArrayList<com.muhammadfadhlidzilikram.latihan_crud_retrofit_pet.Pets> filterList, com.muhammadfadhlidzilikram.latihan_crud_retrofit_pet.Adapter adapter)
    {
        this.adapter=adapter;
        this.filterList=filterList;

    }

    //FILTERING OCURS
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        //CHECK CONSTRAINT VALIDITY
        if(constraint != null && constraint.length() > 0)
        {
            //CHANGE TO UPPER
            constraint=constraint.toString().toUpperCase();
            //STORE OUR FILTERED PLAYERS
            ArrayList<com.muhammadfadhlidzilikram.latihan_crud_retrofit_pet.Pets> filteredPets=new ArrayList<>();

            for (int i=0;i<filterList.size();i++)
            {
                //CHECK
                if(filterList.get(i).getName().toUpperCase().contains(constraint))
                {
                    //ADD PLAYER TO FILTERED PLAYERS
                    filteredPets.add(filterList.get(i));
                }
            }

            results.count=filteredPets.size();
            results.values=filteredPets;

        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.pets= (ArrayList<com.muhammadfadhlidzilikram.latihan_crud_retrofit_pet.Pets>) results.values;

        //REFRESH
        adapter.notifyDataSetChanged();

    }
}